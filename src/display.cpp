#include <GL/glut.h>
#include <Magick++/Image.h>
#include <Magick++/Color.h>
#include <Magick++/Geometry.h>

#include <vector>
#include <iostream>
#include <string>

#include <assimp/scene.h>

#include "display.h"
#include "rtcimport.h"

using namespace std;

int Display::width = 0;
int Display::height = 0;
aiColor3D** Display::image;

void Display::display() {
    outputImage.display();
}

Display::Display(RTCImport* conf) {
    int argc = 0;
    char** argv;

    Display::width = conf->xres;
    Display::height = conf->yres;
    Display::image = new aiColor3D*[width];
    MagickCore::InitializeMagick("");
    for(int i=0; i<width; i++){
        image[i] = new aiColor3D[height];
        for(int j=0; j<height; j++){
            image[i][j] = aiColor3D(0.f, 0.f, 0.f);
        }
    }
}

void Display::save(string output_file) {
    outputImage.write(output_file);
}

void Display::createImage() {
    outputImage = Magick::Image(Magick::Geometry(width, height), "pink");
    for(unsigned int y=0; y<height; y++){
        for(unsigned int x=0; x<width; x++){
            outputImage.pixelColor(x, y, Magick::Color(
                                                        (int)(image[x][y].r*QuantumRange),
                                                        (int)(image[x][y].g*QuantumRange),
                                                        (int)(image[x][y].b*QuantumRange)
                                                       ));
        }
    }
}
