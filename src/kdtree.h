#ifndef __KDTREE_H_INCLUDED__
#define __KDTREE_H_INCLUDED__

#include <vector>
#include <algorithm>
#include <iostream>

#include "display.h"
#include "scene.h"
#include "stopwatch.h"
#include "mesh.h"

using namespace std;

class KDTreeNode {
    public:
        KDTreeNode* left;
        KDTreeNode* right;
        float split;
        char axis;
        vector<SubMesh> submeshes;
        static int nodes_count;

        void build(Scene *scene);
        void build(int r, AABB &box,
                vector<vector<float> > &sorted_boundaries, 
                vector<vector<float> > &triangle_ends);
        void get_sorted_boundaries(vector<vector<float> > &sorted_boundaries,
                vector<vector<float> > &triangle_ends);
        int count_triangles();
        bool find_split_position_median(int r, AABB &box);
        float split_cost(AABB &box, int axis, int split_position);
        bool find_median(char axis);
        bool find_split_position_sah(AABB &box, 
                vector<vector<float> > &sorted_boundaries, 
                vector<vector<float> > &triangle_ends);
        int intersectTriangle(Ray &ray, Triangle &tr,
            float &t, float &u, float &v);
        bool traverseKDTree(KDTreeNode &node, Ray &ray, AABB &box,
            float &t, float &u, float &v, 
            Triangle **closest_triangle, Mesh **closest_mesh);
        bool rayIntersection(aiVector3D p, aiVector3D dir,
                float *t, float *u, float *v, 
                Triangle **closest_triangle, Mesh **closest_mesh,
                Scene *scene);


};

#endif
