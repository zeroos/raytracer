#include <assimp/scene.h>
#include <sstream>
#include <cmath>

#include "triangle.h"

using namespace std;

Triangle::Triangle(aiVector3D *v1, aiVector3D *v2, aiVector3D *v3) {
    this->v1 = v1;
    this->v2 = v2;
    this->v3 = v3;
    this->visited = false;
}

string Triangle::str() {
    ostringstream out;
    out << "Triangle({" << v1->x << "," << v1->y << "," << v1->z << "}, {" 
                        << v2->x << "," << v2->y << "," << v2->z << "}, {"
                        << v3->x << "," << v3->y << "," << v3->z << "})";
    return out.str();
}

void Triangle::project_to_axis(const aiVector3D &axis, float &dMin, float &dMax) const {
    /* http://clb.demon.fi/MathGeoLib/docs/Triangle.cpp_code.html#459 */
    dMin = dMax = axis * *v1;
    float t = axis * *v2;
    dMin = min(t, dMin);
    dMax = max(t, dMax);
    t = axis * *v3;
    dMin = min(t, dMin);
    dMax = max(t, dMax);
}
