#include <assimp/scene.h>
#include <string>
#include <sstream>
#include <iostream>
#include <cmath>


#include "aabb.h"


bool ranges_overlap(float start1, float end1, float start2, float end2) {
    return end1 >= start2 && end2 >= start1;
}

AABB::AABB() {
    this->v1 = aiVector3D(-INFINITY, -INFINITY, -INFINITY);
    this->v2 = aiVector3D(INFINITY, INFINITY, INFINITY);
}

AABB::AABB(AABB &aabb) {
    this->v1 = aabb.v1;
    this->v2 = aabb.v2;
}

AABB::AABB(aiVector3D w1, aiVector3D w2) {
    v1 = w1;
    v2 = w2;
}

AABB::AABB(Triangle *t) {
    float x1 = min(t->v1->x, min(t->v2->x, t->v3->x));
    float y1 = min(t->v1->y, min(t->v2->y, t->v3->y));
    float z1 = min(t->v1->z, min(t->v2->z, t->v3->z));

    float x2 = max(t->v1->x, max(t->v2->x, t->v3->x));
    float y2 = max(t->v1->y, max(t->v2->y, t->v3->y));
    float z2 = max(t->v1->z, max(t->v2->z, t->v3->z));
    v1 = aiVector3D(x1, y1, z1);
    v2 = aiVector3D(x2, y2, z2);
}

string AABB::str() const {
    ostringstream out;
    out << "AABB({" << v1.x << "," << v1.y << "," << v1.z << "}, {" 
                    << v2.x << "," << v2.y << "," << v2.z << "})";
    return out.str();
}

bool AABB::has_on_border(Triangle *t) {
    for(int i=0; i<3; i++) { //for each dimension
        if((*(t->v1))[i] == (*(t->v2))[i] &&
           (*(t->v2))[i] == (*(t->v3))[i] &&
            ((*(t->v3))[i] == this->v1[i] ||
             (*(t->v3))[i] == this->v2[i])
          ){
            return true;
        }
    }
    return false;
}

bool AABB::has_inside(aiVector3D &p) {
    for(int i=0; i<3; i++) { //for each dimension
        if(p[i] > this->v2[i] || p[i] < this->v1[i]){
            return false;
        }
    }
    return true;
}

void AABB::project_to_axis(const aiVector3D &axis, float &dMin, float &dMax) const {
    aiVector3D c = center();
    aiVector3D hs = (v2 - v1)*0.5f;
    float r = abs(hs[0]*abs(axis[0]) + hs[1]*abs(axis[1]) + hs[2]*abs(axis[2]));
    float s = axis * c;
    dMin = s - r;
    dMax = s + r;
}

bool AABB::intersects(Triangle *t) {
    float t1, t2, a1, a2;
    const aiVector3D e[3] = { aiVector3D(1, 0, 0), 
                       aiVector3D(0, 1, 0),
                       aiVector3D(0, 0, 1) };

    for(int i=0; i<3; ++i) {
        t->project_to_axis(e[i], t1, t2);
        this->project_to_axis(e[i], a1, a2);
        if(!ranges_overlap(t1, t2, a1, a2)) {
            //cout << "FALSE a" << i << " " << t1 << " " << t2 << " " << a1 << " " << a2 << endl;
            return false;
        }
        
    }

    aiVector3D n = (*(t->v2) - *(t->v1))^(*(t->v3) - *(t->v1));

    t->project_to_axis(n, t1, t2);
    this->project_to_axis(n, a1, a2);
    if (!ranges_overlap(t1, t2, a1, a2)) {
        //cout << "FALSE B" << endl;
        return false;
    }

    const aiVector3D ti[3] = { *(t->v2) - *(t->v1), *(t->v3) - *(t->v1), *(t->v3) - *(t->v2) };
    for(int i = 0; i < 3; ++i){
        for(int j = 0; j < 3; ++j){
            aiVector3D axis = e[i]^ti[j];
            float len = axis.SquareLength();
            if (len <= 1e-4f){
                //cout << "cont" << endl;
                continue; // Ignore tests on degenerate axes.
            }
    
            t->project_to_axis(axis, t1, t2);
            this->project_to_axis(axis, a1, a2);
            if (!ranges_overlap(t1, t2, a1, a2)){
                //cout << "FALSE c" << endl;
                return false;
            }
        }
    }
    
    //cout << "TRUE" << endl;
    // No separating axis exists, the AABB and triangle intersect.
    return true;


     
    //AABB aabb(t);
    //return this->intersects(&aabb);
}

bool AABB::intersects(AABB *t) {
    if(this->v1.x >= t->v2.x || this->v2.x <= t->v1.x) {
        return false;
    }
    if(this->v1.y >= t->v2.y || this->v2.y <= t->v1.y) {
        return false;
    }
    if(this->v1.z >= t->v2.z || this->v2.z <= t->v1.z) {
        return false;
    }
    /*if(this->v2.x < t->v1.x ||
       this->v2.y < t->v1.y ||
       this->v1.x > t->v2.x ||
       this->v1.y > t->v2.y){
        return false;
    }*/

    return true;
}

float AABB::intersectDistance(Ray &ray) {
    //float tmin, tmax, t1, t2, tymin, tymax, tzmin, tzmax;

    if(this->has_inside(ray.orig)) {
        return 0;
    }

    /*aiVector3D invdir = aiVector3D(1/ray->dir.x, 1/ray->dir.y, 1/ray->dir.z);

    t1 = (this->v1.x - ray->orig.x) * invdir.x;
    t2 = (this->v2.x - ray->orig.x) * invdir.x;

    tmin = min(t1, t2);
    tmax = max(t1, t2);

    t1 = (this->v1.y - ray->orig.y) * invdir.y;
    t2 = (this->v2.y - ray->orig.y) * invdir.y;

    tymin = min(t1, t2);
    tymax = max(t1, t2);

    if ((tmin > tymax) || (tymin > tmax))
        return INFINITY;
    if (tymin > tmin)
        tmin = tymin;
    if (tymax < tmax)
        tmax = tymax;

    t1 = (this->v1.z - ray->orig.z) * invdir.z;
    t2 = (this->v2.z - ray->orig.z) * invdir.z;

    tzmin = min(t1, t2);
    tzmax = max(t1, t2);

    if ((tmin > tzmax) || (tzmin > tmax))
        return INFINITY;
    if (tzmin > tmin)
        tmin = tzmin;
    /*if (tzmax < tmax)
        tmax = tzmax;*/

    float t1,t2,tmin = -1000.0f,tmax = 1000.0f,temp,tCube;
    aiVector3D b1 = this->v1;
    aiVector3D b2 = this->v2;
    bool intersectFlag = true;
    for(int i =0 ;i < 3; i++){
        if(ray.dir[i] == 0){
            if(ray.orig[i] < b1[i] || ray.orig[i] > b2[i])
                return INFINITY;
        }
        else{
            t1 = (b1[i] - ray.orig[i])/ray.dir[i];
            t2 = (b2[i] - ray.orig[i])/ray.dir[i];
            if(t1 > t2){
                swap(t1, t2);
            }
            if(t1 > tmin)
                tmin = t1;
            if(t2 < tmax)
                tmax = t2;
            if(tmin > tmax)
                return INFINITY;
            if(tmax < 0)
                return INFINITY;
        }
    }
    return tmin;

}

float AABB::volume() const {
    aiVector3D diff = (v2-v1);
    return diff.x * diff.y * diff.z;
}

float AABB::surface_area() const {
    aiVector3D diff = (v2-v1);
    return diff.x*diff.y*2 + diff.x*diff.z*2 + diff.z*diff.y*2;
}

aiVector3D AABB::center() const {
    //return aiVector3D(v1);
    return (v1/2.0f + v2/2.0f);
}

