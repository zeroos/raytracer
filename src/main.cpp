#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

#include <iostream>
#include <cstdio>
#include <unistd.h>

#include "display.h"
#include "raytracer.h"
#include "rtcimport.h"
#include "stopwatch.h"

using namespace std;

clock_t clock_start;


bool importObjects(RTCImport *conf, Display d)
{
    // Create an instance of the Importer class
    Assimp::Importer importer;
    // And have it read the given file with some example postprocessing
    // Usually - if speed is not the most important aspect for you - you'll 
    // propably to request more postprocessing than we do in this example.
    const aiScene* scene = importer.ReadFile( conf->objfile, 
          aiProcess_CalcTangentSpace       | 
          aiProcess_Triangulate            |
          aiProcess_JoinIdenticalVertices  |
          aiProcess_SortByPType);
    
    // If the import failed, report it
    if( !scene)
    {
        cout << importer.GetErrorString() << endl;
        return false;
    }
    // Now we can access the file's contents. 
    Scene s(scene, &conf->lights);
    cout << "scene loaded    (" << Stopwatch::timestamp() << ")" << endl;
    Raytracer r = Raytracer(&s, d, conf);
    double t = Stopwatch::timedelta();
    cout << "image generated (" << Stopwatch::timestamp() << ")" << endl;
    
    cout << r.ray_count << " rays casted (" << int(floor(r.ray_count/t)) << "/s)" << endl;
    
    // We're done. Everything will be cleaned up by the importer destructor
    return true;
}

int main(int argc, char * argv[])
{
    RTCImport conf = RTCImport(argv[1]);
    Display d = Display(&conf);

    Stopwatch::start();

    importObjects(&conf, d);
    d.createImage();

    d.save(conf.pngfile);

    if(argc > 2) {
        d.display();
    }

    return 0;
}
