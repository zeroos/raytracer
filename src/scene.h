#ifndef __SCENE_H_INCLUDED__
#define __SCENE_H_INCLUDED__

#include <cmath>
#include <vector>
#include <assimp/scene.h>

#include "triangle.h"
#include "aabb.h"
#include "mesh.h"
#include "ray.h"

using namespace std;

class KDTreeNode;

class Scene {
    private:
        const aiScene* ai_scene;
        void recursive_traverse(const aiNode* nd);
        void set_aabb();
    public:
        vector<Mesh*> meshes;
        vector<Light*> lights;
        Scene(const aiScene* s, vector<aiVector3D> *lights_pos);
        AABB aabb;
};

#endif
