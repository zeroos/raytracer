#include "kdtree.h"


#define EPSILON 0.000001
#define CROSS(dest,v1,v2) \
          dest[0]=v1[1]*v2[2]-v1[2]*v2[1]; \
          dest[1]=v1[2]*v2[0]-v1[0]*v2[2]; \
          dest[2]=v1[0]*v2[1]-v1[1]*v2[0];
#define DOT(v1,v2) (v1[0]*v2[0]+v1[1]*v2[1]+v1[2]*v2[2])
#define SUB(dest,v1,v2) \
          dest[0]=v1[0]-v2[0]; \
          dest[1]=v1[1]-v2[1]; \
          dest[2]=v1[2]-v2[2]; 



#ifndef TRAVERSAL_COST
    #define TRAVERSAL_COST 21
    #define INTERSECTION_COST 1
#endif

using namespace std;

int KDTreeNode::nodes_count = 0;

void KDTreeNode::build(Scene* scene) {
    for(int i=0; i<scene->meshes.size(); i++){
        SubMesh s(scene->meshes[i]);
        s.full();
        submeshes.push_back(s);
    }

    vector<vector<float> > sorted_boundaries;
    vector<vector<float> > triangle_ends;
    get_sorted_boundaries(sorted_boundaries, triangle_ends);

    build(0, scene->aabb, sorted_boundaries, triangle_ends);

    cout << "KDTree build    (" << Stopwatch::timestamp() << ")" << endl;
    cout << nodes_count << " nodes" << endl;
}

void KDTreeNode::build(int r, AABB &box, 
        vector<vector<float> > &sorted_boundaries,
        vector<vector<float> > &triangle_ends) {
    left = NULL;
    right = NULL;
    /*cout << "---" << endl;
    cout << box.str() << endl;
    cout << submeshes.size() << " " << count_triangles() << endl;
    cout << "r: " << r << " vol: " << box.volume() << endl;*/
    nodes_count++;
    if((count_triangles() < 15 ) || r > 100 || box.volume() < 0.01) {
        //cout << "leaf" << endl;
        return;
    }

    /* this could be deduced from previous data */
    vector<vector<float> > sorted_boundaries1;
    vector<vector<float> > triangle_ends1;
    get_sorted_boundaries(sorted_boundaries1, triangle_ends1);

    if(!find_split_position_sah(box, sorted_boundaries1, triangle_ends1)) {
    //if(!find_split_position_median(r, box)) {
        //cout << "also leaf" << endl;
        return;
    }

    //cout << "Split pos: " << split << " (" << (int)axis << ") axis" << endl;
    
    left = new KDTreeNode();
    right = new KDTreeNode();

    AABB lbox(box);
    AABB rbox(box);
    
    lbox.v2[axis] = split;
    rbox.v1[axis] = split;

    while(submeshes.size()>0){
        SubMesh submesh = submeshes.back();
        submeshes.pop_back();
        SubMesh lsubmesh(submesh.mesh);
        SubMesh rsubmesh(submesh.mesh);

        //cout << "submesh" << endl;
        while(submesh.triangles.size()>0){
            Triangle* t = submesh.triangles.back();
            submesh.triangles.pop_back();
            
            //cout << t->str() << endl; bool f = false;
            if(lbox.intersects(t) || lbox.has_on_border(t)){
                lsubmesh.triangles.push_back(t);
                //cout << "l" << endl;  f = true;
            }
            if(rbox.intersects(t)){
                rsubmesh.triangles.push_back(t);
                //cout << "r" << endl; f = true;
            }
            //if(!f) cout << "ERRRR" << endl;
        }
        if(!lsubmesh.triangles.empty()) left->submeshes.push_back(lsubmesh);
        if(!rsubmesh.triangles.empty()) right->submeshes.push_back(rsubmesh);
    }
    left->build(r+1, lbox, sorted_boundaries, triangle_ends);
    right->build(r+1, rbox, sorted_boundaries, triangle_ends);
}

void KDTreeNode::get_sorted_boundaries(vector<vector<float> > &sorted_boundaries,
                vector<vector<float> > &triangle_ends) {
    sorted_boundaries.resize(3);
    triangle_ends.resize(3);
    for(int axis=0; axis<3; axis++) {
        for(int i=0; i<submeshes.size(); i++){
            for(int j=0; j<submeshes[i].triangles.size(); j++){
                float v1 = (*(submeshes[i].triangles[j]->v1))[axis];
                float v2 = (*(submeshes[i].triangles[j]->v2))[axis];
                float v3 = (*(submeshes[i].triangles[j]->v3))[axis];

                float l_min = min(v1, min(v2, v3));
                float l_max = max(v1, max(v2, v3));

                sorted_boundaries[axis].push_back(l_min);
                sorted_boundaries[axis].push_back(l_max);
                triangle_ends[axis].push_back(l_max);
            }
        }
        sort (sorted_boundaries[axis].begin(), sorted_boundaries[axis].end());
        sort (triangle_ends[axis].begin(), triangle_ends[axis].end());
    }
}

int KDTreeNode::count_triangles() {
    int count = 0;
    for(int i=0; i<submeshes.size(); i++){
        count += submeshes[i].triangles.size();
    }
    return count;
}

bool KDTreeNode::find_split_position_sah(AABB &box, 
        vector<vector<float> > &sorted_boundaries,
        vector<vector<float> > &triangle_ends) {
    
    int triangles_count = count_triangles();
    float box_sa = box.surface_area();

    int opt_axis = 0;
    float opt_split = 0;
    float opt_cost = INFINITY;

    for(int axis=0; axis<3; axis++) {
        int triangles_left = 0;
        for(int i=0; i<sorted_boundaries[axis].size(); i++){
            float split_position = sorted_boundaries[axis][i];

            AABB lbox(box);
            AABB rbox(box);
    
            lbox.v2[axis] = split_position;
            rbox.v1[axis] = split_position;

            int triangles_right = triangles_count - triangles_left;

            float cost = TRAVERSAL_COST + INTERSECTION_COST*(
                    (lbox.surface_area()/box_sa*triangles_left) + 
                    (rbox.surface_area()/box_sa*triangles_right));

            if(cost < opt_cost) {
                opt_axis = axis;
                opt_split = split_position;
                opt_cost = cost;
            }
            if(split_position == triangle_ends[axis][triangles_left]){
                ++triangles_left;
            }
        }
    }
    //cout << opt_cost << endl;
    this->split = opt_split;
    this->axis = opt_axis;
    return (opt_cost < INTERSECTION_COST * triangles_count);
}

bool KDTreeNode::find_split_position_median(int r, AABB &box) {
    axis = r%3;
    aiVector3D diff = box.v2 - box.v1;
    while(diff[axis] == 0 && axis < 3) ++axis;

    while(axis < 3 && !find_median(axis)) ++axis;
    
    return (axis < 3);
}

bool KDTreeNode::find_median(char axis) {
    vector<float> p;
    float mini = INFINITY;
    float maxi = -INFINITY;
    for(int i=0; i<submeshes.size(); i++){
        for(int j=0; j<submeshes[i].triangles.size(); j++){
            float v1 = (*(submeshes[i].triangles[j]->v1))[axis];
            float v2 = (*(submeshes[i].triangles[j]->v2))[axis];
            float v3 = (*(submeshes[i].triangles[j]->v3))[axis];

            float l_min = min(v1, min(v2, v3));
            float l_max = max(v1, max(v2, v3));
            p.push_back(l_min);
            p.push_back(l_max);
            if(l_min < mini) mini = l_min;
            if(l_max > maxi) maxi = l_max;
        }
    }
    int n = p.size()/2;
    nth_element(p.begin(), p.begin()+n, p.end());
    
    split = p[n];

    return !(split == maxi || split == mini);
}

bool KDTreeNode::traverseKDTree(KDTreeNode &node, Ray &ray, AABB &box,
        float &t, float &u, float &v, Triangle **closest_triangle,
        Mesh **closest_mesh) {

    if(node.left == NULL){ //right must also be NULL
        bool found = false;
        for(int i=0; i<node.submeshes.size(); i++){
            for(int j=0; j<node.submeshes[i].triangles.size(); j++){
                Triangle *triangle = node.submeshes[i].triangles[j];
                float rt, ru, rv;
                int in = intersectTriangle(ray, *triangle, 
                            rt, ru, rv);
                if(in > 0){
                    if(rt > 0 && (!found || t > rt)){
                        t = rt;
                        u = ru;
                        v = rv;
                        *closest_mesh = node.submeshes[i].mesh;
                        *closest_triangle = triangle;
                        found = true;
                    }
                }
            }
        }
        return found;
    }else{
        KDTreeNode *closer = node.left;
        KDTreeNode *further = node.right;

        AABB lbox(aiVector3D(box.v1.x, box.v1.y, box.v1.z),
              aiVector3D(box.v2.x, box.v2.y, box.v2.z));
        AABB rbox(aiVector3D(box.v1.x, box.v1.y, box.v1.z),
              aiVector3D(box.v2.x, box.v2.y, box.v2.z));
        
        lbox.v2[node.axis] = node.split;
        rbox.v1[node.axis] = node.split;

        AABB *box1 = &lbox;
        AABB *box2 = &rbox;

        float t1 = box1->intersectDistance(ray);
        float t2 = box2->intersectDistance(ray);

        if(t2 < t1){
            swap(t1, t2);
            swap(closer, further);
            swap(box1, box2);
        }

        if(t1 == INFINITY){
            return false;
        }
        
        float rt1 = INFINITY, ru1, rv1;
        float rt2 = INFINITY, ru2, rv2;
        Triangle *closest_triangle1, *closest_triangle2;
        Mesh *closest_mesh1, *closest_mesh2;

        bool found_1 = traverseKDTree(*closer, ray, *box1,
                rt1, ru1, rv1, &closest_triangle1, &closest_mesh1);

        bool found_2 = false;

        if(t2 != INFINITY && t2 < rt1) {
            found_2 = traverseKDTree(*further, ray, *box2,
                    rt2, ru2, rv2, &closest_triangle2, &closest_mesh2);
        }

        if(rt1 <= rt2) {
            *closest_triangle = closest_triangle1;
            *closest_mesh = closest_mesh1;
            t = rt1;
            u = ru1;
            v = rv1;
        }else{
            *closest_triangle = closest_triangle2;
            *closest_mesh = closest_mesh2;
            t = rt2;
            u = ru2;
            v = rv2;
        }

        return (found_1 || found_2);
    }
}

bool KDTreeNode::rayIntersection(aiVector3D p, aiVector3D pu,
        float *t, float *u, float *v, 
        Triangle **closest_triangle, Mesh **closest_mesh,
        Scene *scene) {

    Ray ray = Ray(p, pu);
    return traverseKDTree(*this, ray, scene->aabb,
        *t, *u, *v, closest_triangle, closest_mesh);

}

int KDTreeNode::intersectTriangle(Ray &ray, Triangle &tr,
        float &t, float &u, float &v){
    float edge1[3], edge2[3], tvec[3], pvec[3], qvec[3];
    float det,inv_det;

    float orig[3] = {ray.orig.x, ray.orig.y, ray.orig.z};
    float dir[3] = {ray.dir.x, ray.dir.y, ray.dir.z};
    float vert0[3] = {tr.v1->x, tr.v1->y, tr.v1->z};
    float vert1[3] = {tr.v2->x, tr.v2->y, tr.v2->z};
    float vert2[3] = {tr.v3->x, tr.v3->y, tr.v3->z};


    /* find vectors for two edges sharing vert0 */
    SUB(edge1, vert1, vert0);
    SUB(edge2, vert2, vert0);

    /* begin calculating determinant - also used to calculate U parameter */
    CROSS(pvec, dir, edge2);

    /* if determinant is near zero, ray lies in plane of triangle */
    det = DOT(edge1, pvec);

    if (det > EPSILON)
    {
        /* calculate distance from vert0 to ray origin */
        SUB(tvec, orig, vert0);

        /* calculate U parameter and test bounds */
        u = DOT(tvec, pvec);
        if (u < 0.0 || u > det)
            return 0;

        /* prepare to test V parameter */
        CROSS(qvec, tvec, edge1);

        /* calculate V parameter and test bounds */
        v = DOT(dir, qvec);
        if (v < 0.0 || u + v > det)
            return 0;

    }
    else if(det < -EPSILON)
    {
        /* calculate distance from vert0 to ray origin */
        SUB(tvec, orig, vert0);

        /* calculate U parameter and test bounds */
        u = DOT(tvec, pvec);
        if (u > 0.0 || u < det)
            return 0;

        /* prepare to test V parameter */
        CROSS(qvec, tvec, edge1);

        /* calculate V parameter and test bounds */
        v = DOT(dir, qvec) ;
        if (v > 0.0 || u + v < det)
            return 0;
    }
    else return 0;  /* ray is parallell to the plane of the triangle */


    inv_det = 1.0 / det;

    /* calculate t, ray intersects triangle */
    t = DOT(edge2, qvec) * inv_det;
    u *= inv_det;
    v *= inv_det;

    return 1;
}

