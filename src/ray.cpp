#include <assimp/scene.h>
#include <sstream>
#include <iostream>

#include "ray.h"


Ray::Ray(aiVector3D orig, aiVector3D dir) {
    this->orig = orig;
    this->dir = dir;
}

string Ray::str() {
    ostringstream out;
    out << "Ray({" << orig.x << "," << orig.y << "," << orig.z << "} + a{" 
                   << dir.x  << "," << dir.y  << "," << dir.z << "})";
    return out.str();
}
