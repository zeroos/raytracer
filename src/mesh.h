#ifndef __MESH_H_INCLUDED__
#define __MESH_H_INCLUDED__

#include <vector>
#include <assimp/scene.h>

#include "triangle.h"

using namespace std;

class Light {
    public:
        aiColor3D color_diffuse, color_specular, color_ambient;
        aiVector3D pos;
        Light(aiLight* l);
        Light(aiVector3D pos, aiColor3D color_diffuse, aiColor3D color_ambient, aiColor3D color_specular);
};

class Mesh {
    public:
        aiColor3D color_diffuse, color_specular, color_ambient, color_emissive, color_transparent;
        float shininess;
        float reflectivity;
        vector<aiVector3D> vertices;
        vector<Triangle> triangles;
        Mesh(aiMesh* m, const aiMatrix4x4* transformation, const aiScene* scene);
};

class SubMesh {
    public:
        Mesh* mesh;
        vector<Triangle*> triangles;
        SubMesh(Mesh* mesh);
        void full();
};

#endif
