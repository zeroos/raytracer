#ifndef __STOPWATCH_H_INCLUDED__
#define __STOPWATCH_H_INCLUDED__


#include <ctime>
#include <string>

using namespace std;

class Stopwatch {
    private:
        static clock_t start_time;
        static clock_t last_stop;
    public:
        static double timedelta();
        static void start();
        static string timestamp();
};


#endif
