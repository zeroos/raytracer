#ifndef __AABB_H_INCLUDED__
#define __AABB_H_INCLUDED__

#include <string>
#include "triangle.h"
#include "ray.h"

using namespace std;

class AABB {
    public:
        aiVector3D v1;
        aiVector3D v2;
        AABB();
        AABB(AABB &aabb);
        AABB(aiVector3D v1, aiVector3D v2);
        AABB(Triangle *t);
        string str() const;
        bool has_on_border(Triangle *t);
        bool has_inside(aiVector3D &p);
        void project_to_axis(const aiVector3D &axis, float &dMin, float &dMax) const;
        bool intersects(Triangle *t);
        bool intersects(AABB *aabb);
        float intersectDistance(Ray &ray);
        float volume() const;
        float surface_area() const;
        aiVector3D center() const;
};

#endif
