#ifndef __TRIANGLE_H_INCLUDED__
#define __TRIANGLE_H_INCLUDED__

using namespace std;

class Triangle {
    public:
        aiVector3D* v1;
        aiVector3D* v2;
        aiVector3D* v3;
        Triangle(aiVector3D *v1, aiVector3D *v2, aiVector3D *v3);
        string str();
        void project_to_axis(const aiVector3D &axis, float &dMin, float &dMax) const;
        bool visited;
};


#endif
