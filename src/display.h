#ifndef __DISPLAY_H_INCLUDED__
#define __DISPLAY_H_INCLUDED__

#include <Magick++/Image.h>

#include "rtcimport.h"

using namespace std;

class Display{
    private:
        Magick::Image outputImage;
    public:
        static aiColor3D** image;
        static int width, height;
        Display(RTCImport* conf);
        void display();
        void save(string output_file);
        void createImage();
};
#endif
