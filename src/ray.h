#ifndef __RAY_H_INCLUDED__
#define __RAY_H_INCLUDED__

using namespace std;

class Ray {
    public:
        aiVector3D orig;
        aiVector3D dir;
        Ray(aiVector3D orig, aiVector3D dir);
        string str();
};

#endif
