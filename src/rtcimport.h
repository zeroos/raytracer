#ifndef __RTCIMPORT_H_INCLUDED__
#define __RTCIMPORT_H_INCLUDED__

#include <assimp/scene.h>
#include <vector>

using namespace std;

class RTCImport {
    public:
        RTCImport(char * filename);
        string objfile;
        string pngfile;
        float k;
        float xres;
        float yres;
        aiVector3D VP;
        aiVector3D LA;
        aiVector3D UP;
        float yview;
        vector<aiVector3D> lights;
        int samples_per_px;
};


#endif
