#include "scene.h"

using namespace std;

void Scene::recursive_traverse(const aiNode* nd) {
    for(int i=0; i<nd->mNumMeshes; i++){
        this->meshes.push_back(new Mesh(ai_scene->mMeshes[nd->mMeshes[i]], &nd->mTransformation, ai_scene));
    }
    for(int i=0; i<nd->mNumChildren; i++) {
        this->recursive_traverse(nd->mChildren[i]);
    }
}

Scene::Scene(const aiScene* s, vector<aiVector3D> *lights_pos) {
    ai_scene = s;
    for(int i=0; i<s->mNumLights; i++) {
        this->lights.push_back(new Light(s->mLights[i]));
    }
    for(int i=0; i<lights_pos->size(); i++) {
        aiVector3D l = lights_pos->at(i);
        this->lights.push_back(new Light(l, aiColor3D(1,1,1), aiColor3D(1,1,1), aiColor3D(1,1,1)));
    }
    if(this->lights.size() == 0) {
        this->lights.push_back(new Light(aiVector3D(3, 3, -10), aiColor3D(1,1,1), aiColor3D(1,1,1), aiColor3D(1,1,1)));
        this->lights.push_back(new Light(aiVector3D(-3, 3, -10), aiColor3D(1,1,1), aiColor3D(1,1,1), aiColor3D(1,1,1)));
    }
    this->recursive_traverse(s->mRootNode);

    set_aabb();
}

void Scene::set_aabb() {
    aiVector3D v1 = aiVector3D(INFINITY, INFINITY, INFINITY);
    aiVector3D v2 = aiVector3D(-INFINITY, -INFINITY, -INFINITY);
    for(int i=0; i<this->meshes.size(); i++) {
        for(int j=0; j<this->meshes[i]->triangles.size(); j++) {
            Triangle t = this->meshes[i]->triangles[j];
            v1.x = min(min(v1.x, t.v1->x), min(t.v2->x, t.v3->x));
            v1.y = min(min(v1.y, t.v1->y), min(t.v2->y, t.v3->y));
            v1.z = min(min(v1.z, t.v1->z), min(t.v2->z, t.v3->z));

            v2.x = max(max(v2.x, t.v1->x), max(t.v2->x, t.v3->x));
            v2.y = max(max(v2.y, t.v1->y), max(t.v2->y, t.v3->y));
            v2.z = max(max(v2.z, t.v1->z), max(t.v2->z, t.v3->z));
        }
    }
    aabb = AABB(v1, v2);
}
