#include "raytracer.h"

using namespace std;

void printColor(aiColor3D v) {
    cout << "rgb(" << v.r << "," << v.g << "," << v.b << ")" << endl;
}

aiColor3D Raytracer::getRayColor(aiVector3D p, aiVector3D u, int recursion_num) {
    float min_t, min_u, min_v;
    int min_j;
    Mesh *closest_mesh;
    Triangle *triangle;
    if(!kdtree.rayIntersection(p, u, &min_t, &min_u, &min_v, &triangle, &closest_mesh, scene)){
        ray_count++;
        return aiColor3D(0, 0, 0);
    }
    aiVector3D v1 = *triangle->v1;
    aiVector3D v2 = *triangle->v2;
    aiVector3D v3 = *triangle->v3;
    aiVector3D ppos = (1-min_u-min_v)*v1+min_u*v2+min_v*v3;
    aiVector3D normal = (v2-v1)^(v3-v1);
    aiVector3D nu = u.Normalize();
    normal = normal.Normalize();

    aiColor3D color = closest_mesh->color_ambient; //ambient
    float e = 0.001;
    for(int k=0; k<scene->lights.size(); k++){
        aiVector3D lightPos = scene->lights[k]->pos;
        aiVector3D lightRay = lightPos - ppos;
        aiVector3D reflectedLightRay = 2*(lightRay*normal)*normal - lightRay;
        reflectedLightRay = reflectedLightRay.Normalize();

        float min_t2;

        float lightDistance = lightRay.Length();
        aiVector3D dis = lightRay.Normalize()*e;
        Mesh *blocking_mesh;
        if(kdtree.rayIntersection(ppos+dis, lightRay, &min_t2, &min_u, &min_v, &triangle, &blocking_mesh, scene)){
            ray_count++;
            if(lightDistance > min_t2){
                continue;
            }
        }
        float alpha = closest_mesh->shininess;
        color = color + closest_mesh->color_diffuse*abs(lightRay*normal); //diffuse
        color = color + closest_mesh->color_specular*pow(abs(reflectedLightRay*(-nu)), alpha); //specular
    }

    if(recursion_num > 0){
        aiVector3D reflectedRay = -2*(u*normal)*normal - u;
        reflectedRay.Normalize();
        aiColor3D reflectedRatio = closest_mesh->color_specular;

        aiVector3D dis = reflectedRay*e;
        color = color + reflectedRatio*this->getRayColor(ppos+dis, reflectedRay, recursion_num-1);
    }
    return color;
}

void Raytracer::raytracer_thread(Raytracer *raytracer, int &part, int &part_size, int &parts_x, int &parts_num, 
        Display &d, RTCImport *&conf,
        float &e, aiVector3D &vx, aiVector3D &vy, aiVector3D &vz, mutex &mtx) {
    while(true){
        int my_part;
        mtx.lock();
        if(part<parts_num){
            my_part = part;
            part++;
            mtx.unlock();
        }else{
            mtx.unlock();
            break;
        }
        if(part%10 == 0) {
            cout << '\r' << 100*part/parts_num << '%';
            cout.flush();
        }
        int y_part_start = floor(my_part/parts_x) * part_size;
        int y_part_end = floor((my_part/parts_x)+1) * part_size;
        for(int y=y_part_start; y<y_part_end && y<d.height; y++){
            float dy = ((float)y - d.height/2) * e;
            int x_part_start = floor(my_part%parts_x) * part_size;
            int x_part_end = floor((my_part%parts_x)+1) * part_size;
            for(int x=x_part_start; x<x_part_end && x<d.width; x++){
                float dx = ((float)x - d.width/2) * e;
                for(int sample_num=0; sample_num<conf->samples_per_px; sample_num++){
                    float s = 0;
                    if(sample_num != 0){
                        s = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/e));
                    }
                    aiColor3D c = raytracer->getRayColor(conf->VP, 
                        (dx+s)*vx + (dy+s)*vy + vz, 
                        conf->k);
                    d.image[x][d.height-1-y] = d.image[x][d.height-1-y] + c * (1/(float)conf->samples_per_px);
                }
            }
        }
    }
}

Raytracer::Raytracer(Scene *s, Display d, RTCImport *conf) {
    ray_count = 0;
    scene = s;

    kdtree.build(s);

    float v_angle = conf->yview;
    float v_angle_tangent = tan(v_angle/2);
    float e = v_angle_tangent/(d.height/2);

    aiVector3D vy = conf->UP;
    aiVector3D vz = conf->LA - conf->VP;
    aiVector3D vx = vy^vz;

    vy.Normalize();
    vx.Normalize();
    vz.Normalize();


    int part_size = 128;
    int parts_x = ceil(((float)d.width)/part_size);
    int parts_num = ceil((float)d.width/part_size)*ceil((float)d.height/part_size);

    unsigned int thread_num = std::thread::hardware_concurrency();

    int part = 0;
    
    thread threads[thread_num];

    mutex mtx;

    Raytracer *rt = this;
    for(int i=0; i<thread_num; i++) {
        thread t(raytracer_thread, rt, ref(part), ref(part_size), ref(parts_x), 
                ref(parts_num), ref(d), ref(conf), ref(e), ref(vx), ref(vy), ref(vz),
                ref(mtx));
        threads[i] = move(t);
    }
    cout << thread_num << " threads started" << endl;
    for(int i=0; i<thread_num; i++) {
        threads[i].join();
    }
    
    cout << "\r    \r";
}
