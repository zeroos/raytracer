#include <ctime>
#include <sstream>
#include <string>
#include <iostream>

#include "stopwatch.h"

using namespace std;

clock_t Stopwatch::start_time = 0;
clock_t Stopwatch::last_stop = 0;

void Stopwatch::start() {
    Stopwatch::start_time = clock();
    Stopwatch::last_stop = Stopwatch::start_time;
}

double Stopwatch::timedelta() {
    clock_t time = clock();
    return double(time - Stopwatch::last_stop)/ CLOCKS_PER_SEC;
}

string Stopwatch::timestamp() {
    ostringstream out;

    clock_t time = clock();
     
    out.setf(std::ios::fixed, std:: ios::floatfield);
    out.precision(4);

    out << double(time - Stopwatch::last_stop) / CLOCKS_PER_SEC << "s/"
        << double(time - Stopwatch::start_time) / CLOCKS_PER_SEC << "s"; 


    Stopwatch::last_stop = time;
    return out.str();
}
