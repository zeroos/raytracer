#ifndef __RAYTRACER_H_INCLUDED__
#define __RAYTRACER_H_INCLUDED__

#include <iostream>
#include <vector>
#include <mutex>
#include <cmath>
#include <thread>
#include <assimp/scene.h>

#include "display.h"
#include "scene.h"
#include "mesh.h"
#include "kdtree.h"
#include "ray.h"
#include "scene.h"
#include "display.h"
#include "kdtree.h"
#include "ray.h"

using namespace std;

class Raytracer {
    private:
        aiColor3D getRayColor(aiVector3D p, aiVector3D u, int recursion_num);
        static void raytracer_thread(Raytracer *raytracer, 
                int &part, int &part_size, int &parts_x, int &parts_num, 
                Display &d, RTCImport *&conf,
                float &e, aiVector3D &vx, aiVector3D &vy, aiVector3D &vz, 
                mutex &mtx);
        KDTreeNode kdtree;
    public:
        long long int ray_count;
        Scene *scene;
        Raytracer(Scene *s, Display d, RTCImport *conf);
};

#endif
