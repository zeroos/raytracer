#include "mesh.h"


Light::Light(aiVector3D pos, aiColor3D color_diffuse, aiColor3D color_ambient, aiColor3D color_specular) {
    this->pos = pos;
    this->color_diffuse = color_diffuse;
    this->color_ambient = color_ambient;
    this->color_specular = color_specular;
}


Light::Light(aiLight* l) {
    this->pos = l->mPosition;
    this->color_diffuse = l->mColorDiffuse;
    this->color_ambient = l->mColorAmbient;
    this->color_specular = l->mColorSpecular;
}


Mesh::Mesh(aiMesh* m, const aiMatrix4x4* t, const aiScene* scene) {
    for(int i=0; i<m->mNumVertices; i++){
        aiVector3D* vi = &m->mVertices[i];

        float x = vi->x*t->a1 + vi->y*t->a2 + vi->z*t->a3 + t->a4;
        float y = vi->x*t->b1 + vi->y*t->b2 + vi->z*t->b3 + t->b4;
        float z = vi->x*t->c1 + vi->y*t->c2 + vi->z*t->c3 + t->c4;
        
        float grid_size = 100000;
        
        x = roundf(x * grid_size) / grid_size;
        y = roundf(y * grid_size) / grid_size;
        z = roundf(z * grid_size) / grid_size;

        aiVector3D v = aiVector3D(x, y, z);
        vertices.push_back(v);
    }
    for(int i=0; i<m->mNumFaces; i++){
        aiFace face = m->mFaces[i];
        triangles.push_back(Triangle(&vertices[face.mIndices[0]], 
                 &vertices[face.mIndices[1]], 
                 &vertices[face.mIndices[2]]));
    }

    aiColor3D color;
    aiMaterial *mat = scene->mMaterials[m->mMaterialIndex];
    mat->Get(AI_MATKEY_COLOR_DIFFUSE, color_diffuse);
    
    mat->Get(AI_MATKEY_COLOR_SPECULAR, this->color_specular);

    mat->Get(AI_MATKEY_COLOR_AMBIENT, this->color_ambient);

    mat->Get(AI_MATKEY_COLOR_EMISSIVE, this->color_emissive);

    mat->Get(AI_MATKEY_COLOR_TRANSPARENT, this->color_transparent);

    mat->Get(AI_MATKEY_SHININESS, this->shininess);

}

SubMesh::SubMesh(Mesh* mesh) {
    this->mesh = mesh;
}

void SubMesh::full() {
    for(int i=0; i<mesh->triangles.size(); i++){
        this->triangles.push_back(&mesh->triangles[i]);
    }
}

