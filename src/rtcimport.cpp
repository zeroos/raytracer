#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include "rtcimport.h"

using namespace std;

RTCImport::RTCImport(char * filename) {
    ifstream f(filename);

    string line;
    istringstream iss;
    float x, y, z;

    samples_per_px = 1;

    if(!std::getline(f, line)) {
        cout << "Empty rtc file" << endl;
        exit(1);
    }
    if(!std::getline(f, line)) {
        cout << "Input file not given" << endl;
        exit(1);
    }
    this->objfile = line;
    if(!std::getline(f, line)) {
        cout << "Output file not given" << endl;
        exit(1);
    }
    this->pngfile = line;
    if(!std::getline(f, line)) {
        cout << "k not given" << endl;
        exit(1);
    }
    iss.str(line);
    iss >> this->k;
    if(!std::getline(f, line)) {
        cout << "Resolution not specified" << endl;
        exit(1);
    }
    iss.str(line);
    iss.clear();
    if (!(iss >> this->xres >> this->yres)) {
        cout << "Incorrect resolution" << endl;
        exit(1);
    }

    if(!std::getline(f, line)) {
        cout << "Viewpoint (VP) not given" << endl;
        exit(1);
    }
    iss.str(line);
    iss.clear();
    if (!(iss >> x >> y >> z)) {
        cout << "Incorrect VP" << endl;
        exit(1);
    }
    this->VP = aiVector3D(x, y, z);

    if(!std::getline(f, line)) {
        cout << "LookAt (LA) not given" << endl;
        exit(1);
    }
    iss.str(line);
    iss.clear();
    if (!(iss >> x >> y >> z)) {
        cout << "Incorrect LA" << endl;
        exit(1);
    }
    this->LA = aiVector3D(x, y, z);
    
    if(std::getline(f, line)) {
        iss.str(line);
        iss.clear();
        if (!(iss >> x >> y >> z)) {
            cout << "Incorrect UP" << endl;
            exit(1);
        }
        this->UP = aiVector3D(x, y, z);
    }else{
        this->UP = aiVector3D(0, 1, 0);
    }

    if(std::getline(f, line)) {
        iss.str(line);
        iss.clear();
        if (!(iss >> this->yview)) {
            cout << "Incorrect yview" << endl;
            exit(1);
        }
    }else{
        this->yview = 1.0;
    }

    
    while (std::getline(f, line))
    {
        char ch;
        float x, y, z;
        iss.str(line);
        iss.clear();
        if (!(iss >> ch >> x >> y >> z)) {
            cout << "Incorrect light" << endl;
            exit(1);
        }
        if(ch == 'l' || ch == 'L') {
            this->lights.push_back(aiVector3D(x, y, z));
        }else if(ch == 's'){
            this->samples_per_px = x;
        }
    }
    cout << filename << endl;
}

