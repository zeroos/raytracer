CPP_FILES := $(wildcard src/*.cpp)
OBJ_FILES := $(addprefix obj/,$(notdir $(CPP_FILES:.cpp=.o)))
LD_FLAGS :=  -lassimp -lstdc++ -lm -lglut -lGL -lGLEW `Magick++-config --ldflags --libs`
#CC_FLAGS := -g -pg `Magick++-config --cxxflags --cppflags` -std=c++11
CC_FLAGS := `Magick++-config --cxxflags --cppflags` -O3 -std=c++11
OUTPUT := bin/raytracer

all:	bin/raytracer
	
bin/raytracer: $(OBJ_FILES)
	gcc $(LD_FLAGS) -s -o $@ $^

obj/%.o: src/%.cpp
	gcc $(CC_FLAGS) -c -o $@ $<

bin/preview:
	gcc $(LD_FLAGS) -lGLU -o bin/preview -s utils/preview.c

.PHONY: clean

clean:
	rm obj/*.o
	rm bin/*
