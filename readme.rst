Raytracer
=========

This project is a raytracer written for "Photorealistic Computer Graphics"
(http://ii.uni.wroc.pl/~anl/dyd/RGK/) course on University of Wrocław by 
Michał Barciś.


Dependencies
------------

 - Magick++ [http://www.imagemagick.org/Magick++/]
 - Assimp [http://assimp.sourceforge.net/]

Building
--------

Type `make` in the main directory. `./bin/raytracer` file should be created.


Running
-------

To run the program type::

    $ ./bin/raytracer file.rtc [display]

The configuration file `file.rtc` would be parsed and output file would be
created. You can also type `display` at the end of command to display the image
after it has been rendered.

RTC file
--------

RTC files are used to configure the scene. The format is pretty simple:

1. line: comment
2. input file to parse (can be any readable by Assimp)
3. output file (any format that ImageMagick can write)
4. k - how many times ray should be reflected (0 means once)
5. width height
6. X Y Z - viewpoint position
7. X Y Z - a point the camera should look at
8. X Y Z - vector pointing upwards
9. yview - view angle

In the next lines you can add ligths by starting a line with `l` and then giving
the position of a light. For example::

    l 5 3 3

You can also set a number of samples per pixel as `s` option. After it you must
put 3 numbers, but only the first one is taken into account, for example::

    s 8 0 0

would set 8 samples per pixel.

Examples
--------
There are some examples in `data` folder, it's also a good place to start. You
can for example render a teapot with the following command::

    $ ./bin/raytracer data/teapot.rtc display

There are also some pre-rendered images, so you can just see how those scenes
look like without even compiling the project.

Have fun!





